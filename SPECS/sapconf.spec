Name:       sapconf
Version:    0.99
Release:    4%{?dist}
Summary:    Prepares a RHEL system for running SAP software

Group:      System Environment/Base
License:    GPLv2+
URL:        http://redhat.com/sap
Source0:    %{name}-%{version}.tar.xz
Patch0:     sapconf-0.99-#1670494-dnf-repoquery-issue.patch
Patch1:     sapconf-0.99-#1701314-function-check_rhn-does-not-cath-major-release-number.patch
Patch2:     sapconf-0.99-#11701246-function-check_links.patch
Patch3:     sapconf-0.99-usage-exit-state.patch
BuildArch:  noarch

Requires:   /bin/bash
# Require tuned versions that include an SAP profile
%if ! 0%{?rhel} || 0%{?rhel} >= 7
# RHEL >= 7
Requires:   tuned > 2.2.2
%else
# RHEL 6
Requires:   tuned >= 0.2.19-13
%endif
Requires:   tuned-profiles-sap
Requires:   uuidd
%ifarch %{ix86} x86_64 ia64 aarch64
Requires:   dmidecode
%endif
Requires:   yum-utils
Requires:   bc
Requires:   bind-utils
# sapconf has been obsoleted. We want to get rid of it from rhel8
# in favor of rhel-system-roles-sap package. Marking it deprecated 
Provides:  deprecated()

%description
This package contains a script that checks and/or modifies a RHEL base
install according to SAP requirements, e.g. kernel parameters, packages etc.,
as documented in SAP Note 2002167 " Red Hat Enterprise Linux 8.x: Installation
and Upgrade".

%prep
%setup -q
%patch0 -p1 -b .#1670494-dnf-repoquery-issue
%patch1 -p1 -b .#1701314-function-check_rhn-does-not-cath-major-release-number
%patch2 -p1 -b .#11701246-function-check_links
%patch3 -p1 -b .usage-exit-state

%build


%install
rm -rf %buildroot
make DESTDIR="%buildroot" install

%clean
rm -rf %buildroot


%files
%defattr(-,root,root,-)
%{_bindir}/sapconf
%{_mandir}/man8/sapconf.8.gz

%doc COPYING README

%changelog
* Thu Aug 06 2020 Than Ngo <than@redhat.com> - 0.99-4
- Resolves: #1866422, mark sapconf deprecated

* Thu May 23 2019 Than Ngo <than@redhat.com> - 0.99-3
- Related: #1681546, fix return value in function usage 

* Thu May 23 2019 Than Ngo <than@redhat.com> - 0.99-2
- Resolves: #1681546, add gating on dist-git tests

* Tue May 21 2019 Than Ngo <than@redhat.com> - 0.99-1
- Resolves: #1701246, disable check_links()
- Resolves: #1670494, workaround for dnf repoquery issues
- Resolves: #1701314, function check_rhn() does not catch major release number
- merged patches and released 0.99

* Mon Nov 06 2017 Than Ngo <than@redhat.com> - 0.98-15
- Resolves: bz#1509921 - sapconf does not identify E4S SAP channel as a SAP channel 

* Mon Nov 06 2017 Than Ngo <than@redhat.com> - 0.98-14
- Resolves: bz#1509913, add missing dependency for bind-utils

* Wed Sep 27 2017 Than Ngo <than@redhat.com> - 0.98-13
- add requirement on bc for sapconf
  Resolves: bz#1496184

* Mon Sep 11 2017 Than Ngo <than@redhat.com> - 0.98-12
- sapconf does not identify EUS SAP channel a s a SAP channel
  Resolves: bz#1470563

- sapconf does not check if limits are set before adding it's own entires
  Resolves: bz#1474024

* Thu Mar 16 2017 Than Ngo <than@redhat.com> - 0.98-11
- sapconf fails on hostnames with exactly 13 characters
  Resolves: bz#1432572

* Fri Nov 11 2016 Than Ngo <than@redhat.com> - 0.98-10
- sapconf cannot go with default values kernel.shmall and kernel.shmmax
  Resolves: bz#1391881

* Tue Sep 20 2016 Than Ngo <than@redhat.com> - 0.98-9
- fix depends on dmidecode on s390x/ppc64 
  Resolves: bz#1377766

* Fri Apr 08 2016 Than Ngo <than@redhat.com> - 0.98-8
- consider ntp configured via ntp package
  Resolves: bz#1228550
- add correct parameter for repoquery
  Resolves: bz#1235608

* Mon May 18 2015 Jan Grulich <jgrulich@redhat.com> - 0.98-7
- Do not wait for input from keyboard and read from file instead
  Resolves: bz#1188233
- Add information about sapconf version
  Resolves: bz#1205269
- Remove unnecessary installed package groups
  Resolves: bz#1193983
- Check whether 99-sap-limits.conf file exists to prevent an error
  Resolves: bz#1205229
- Add force mode
  Resolves: bz#1205691
- Add yum-utils as dependency
  Resolves: bz#1220471

* Mon Dec 08 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-6
- Check minimum required glibc version due to bz#1156331
  Resolves: bz#1170630
- Set correct vm.max_map_count value
  Resolves: bz#1168541
  
* Thu Nov 06 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-5
- add missing tuned-profiles-sap dependency
  resolves: bz##1158409

* Thu Oct 30 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-4
- activate SAP NetWeaver tuned profile (#1158409)
  split patches

* Fri Oct 03 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-3
- prevent from an error from /usr/bin/host when ip address is empty (#1123920)

* Mon Sep 29 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-2
- pickup fixes from git
- mention KVM in manpage (#1077498)
- prevent from an error message when invalid hostname is set (#1123920)

* Thu Jul 17 2014 Jan Grulich <jgrulich@redhat.com> - 0.98-1
- add Man-Page (#1071933)
- correctly detect VMware virtual guest (#1071939)
- do not write to /etc/security/limits.conf directly (#1071942)
- correctly edit /etc/hosts (#1071928, #1083932)
- use a better way how to restore permissions for /etc/hosts (bz#1083932)
- updated SAP Note (#1077502)

* Mon Mar 03 2014 Jan Grulich <jgrulich@redhat.com> - 0.97-2
- check running chronyd instead of ntp
- replace RHEL6 with RHEL7

* Fri Aug 09 2013 Nils Philippsen <nils@redhat.com> - 0.97-1
- use correct mktemp path

* Fri Aug 09 2013 Nils Philippsen <nils@redhat.com> - 0.96-1
- don't use gethostip

* Thu Aug 08 2013 Nils Philippsen <nils@redhat.com> - 0.95-1
- ignore whitespace and empty lines when checking /etc/hosts
- use mktemp to create temporary host file
- add release numbers to changelog entries

* Tue Aug 06 2013 Nils Philippsen <nils@redhat.com> - 0.94-1
- don't use unquoted macros in changelog
- package license text

* Tue Jul 30 2013 Nils Philippsen <nils@redhat.com> - 0.93-1
- remove obsolete tuned patch
- fix changelog dates
- require specific tuned version, prep for upcoming RHEL versions
- clean up script header
- don't mess around with SELinux settings, just report
- use bzipped tarball

* Mon Jul 22 2013 Frank Danapfel <fdanapfe@redhat.com> - 0.93
- replaced vhostmd with vm-dump-metrics for KVM guest installs
- re-added function to set user/process limits
- changed check_ntp to only check if ntpd is running

* Tue Jul 09 2013 Frank Danapfel <fdanapfe@redhat.com> - 0.93
- removed /etc/init.d/sapconf service, since this is now done via tuned
- cleaned up requirements section in spec file
- added changelog to spec file

* Mon Nov 19 2012 Sherry Yu <syu@redhat.com> - 0.92
- Changed the name to sapconf
- Move kernel parameters to /etc/sysconfig/sap, other than /etc/sysctl.conf
- Add /etc/rc.d/init.d/sapconf and S99sapconf links in /etc/rc2.d, /etc/rc3.d,
  and /etc/rc5.d
- Removed check_utilrpms() and defined the util rpms as dependency in the rpm
  spec file
- Removed check_ibmjdk()

* Thu Oct 18 2012 Sherry Yu <syu@redhat.com> - 0.91
- Naming change from sap_prepare to sap-prepare
- Detailed log output
- In function virtrpms: for RHEV Hypervisor, recommend to check SAP Note
  1400911 for setting up host monitoring on RHEV
- New function check_utilrpms: install utility rpms used in the sap-prepare
  script, e.g. syslinux rpm for gethostid()
- Add one section Recommendations at the end of the log file, containing
  recommended changes that have to be carried out manually
- Add a Recommendations section at the end of the log file, for CHECK_ONLY
  mode, that contains the recommended settings
- Add command-line argument "-h" for Help Message
- Move java rpms to variable JAVARPMS, as IBM JDK becomes obsolete on Oct. 2012
- Add global variable VIRTRPMS
- Remove [-f config] argument in usage - recommended configurations are not to
  be changed
- In check_fqdn: keep the backup file as
  /etc/sysconfig/network.sap-prepare-<timestamp>
- In check_security add: if SELINUX=disabled in /etc/sysconfig/selinux, give
  recommendation to set it to Permissive. If SELINUX=permissive but getenforce
  returns 1, setenforce 0. For SELINUX=enforcing, to update the config file,
  keep backup file as /etc/sysconfig/selinux.sap-prepare-<timestamp>; if
  CHECK_ONLY==1, give recommendations
- Split check_rhn into check_rhn, check_rpms, check_ibmjdk, and check_links
- Add a variable SAP_NOTE_RHEL6
- Remove check_users which is not used to keep the script clean
- Remove variable VERBOSE
- Rename prepare_environment to check_env, to be consistent with the naming
  convention
- Move the code of setting kernel parameters from main body to check_env
- Add SYB_VARS
- In check_env add the setting of kernel parameters for Sybase
- In check_ntp ntpdate is removed as ntpd is preferred
- Remove update_limits_conf because it's no longer needed in RHEL6

* Thu Oct 04 2012 Markus Koch <mkoch@redhat.com> - 0.9
- included config file in script, -d database option added, ntp check,  hosts
  check implemented

* Wed Oct 03 2012 Markus Koch <mkoch@redhat.com> - 0.8
- package renamed to sapprepare, vhostmd und logging added

* Wed Apr 18 2012 Frank Danapfel <fdanapfe@redhat.com> - 0.6
- bug fixes

* Tue Feb 21 2012 Markus Koch <mkoch@redhat.com> - 0.5
- initial working release

